const botDoorPath = "https://s3.amazonaws.com/codecademy-content/projects/chore-door/images/robot.svg";

const beachDoorPath = "https://s3.amazonaws.com/codecademy-content/projects/chore-door/images/beach.svg";

const spaceDoorPath = "https://s3.amazonaws.com/codecademy-content/projects/chore-door/images/space.svg";

const numClosedDoors = 3;

let openDoor1 = beachDoorPath;

let openDoor2 = spaceDoorPath;

let openDoor3 = botDoorPath;


let doorImage1 =
    document.getElementById("door1");

//Robot Door//
doorImage1.onclick = () => {
    doorImage1.src = openDoor1;

    //Beach Door//
    doorImage2.onclick = () => {
        doorImage2.src = openDoor2;
    }

    //Space Door//
    doorImage3.onclick = () => {
        doorImage3.src = openDoor3;
    }
};

const randomChoreDoorGenerator = () => {
    const choreDoor = Math.floor(Math.random() * numClosedDoors);
    if (choreDoor === 0) {
        openDoorA = botDoorPath;
        openDoorB = beachDoorPath;
        openDoorC = spaceDoorPath;

    } else if (choreDoor === 1) {
        openDoorB = botDoorPath;
        openDoorA = spaceDoorPath;
        openDoorC = beachDoorPath;

    } else if (choreDoor === 2) {
        openDoorC = botDoorPath;
        openDoorA = beachDoorPath;
        openDoorC = spaceDoorPath;

    }

}



let doorImage2 =
    document.getElementById("door2");

let doorImage3 =
    document.getElementById("door3");

randomDoorGenerator();